use serde::{Deserialize, Serialize};

/// Represents a customer
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Customer {
    /// A unique identifier for a customer record
    pub guid: String,

    /// First name
    pub first_name: String,

    /// Last name
    pub last_name: String,

    /// Email address
    pub email: String,

    /// Physical address
    pub address: String,
}

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct DARP_record {  
    /// A unique identifier for a customer record
    pub group: String,
    /// 
    pub geo: String,
    /// 
    pub public_key: String,
    /// 
    pub mint: String,
    /// IP Address to reach node
    pub ip: String,
    /// Port number to communicate with this node
    pub port: String,
    /// 
    pub version: String,
    /// 
    pub last_pulse_timestamp: String,
    /// 
    pub seq: String,
    /// 
    pub boot_timestamp: String,
    /// 
    pub owls: String,
    /// 
    pub in_pulses: String,    
    /// 
    pub out_pulses: String,      
    /// 
    pub relay_count: String, 
    ///
    pub pkt_drops: String, 
    ///
    pub history: String, 
    ///
    pub median_history: String,         
    ///
    pub last_message: String,         
    ///
    pub state: String,         
    ///
    pub guid: String,    //Unique identifier... right now uniquely identify an end point's particiption in the system =PUBLIC_KEY@GEO:GroupNme

}