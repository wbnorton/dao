use std::convert::Infallible;

use warp::{self, http::StatusCode};

use crate::db::Db;
use crate::models::DARP_record;

/// Returns a list of DARP_records as JSON
///
/// # Arguments
///
/// * `db` - `Db` -> thread safe vector of DARP_record objects
pub async fn list_DARP_records(db: Db) -> Result<impl warp::Reply, Infallible> {
    let DARP_records = db.lock().await;
    let DARP_records: Vec<DARP_record> = DARP_records.clone();
    Ok(warp::reply::json(&DARP_records))
}

/// Creates a new DARP_record
///
/// Adds a new DARP_record object to the data store if the DARP_record
/// doesn't already exist
///
/// # Arguments
///
/// * `new_DARP_record` - `DARP_record` type
/// * `db` - `Db` -> thread safe vector of DARP_record objects
pub async fn create_DARP_record(
    new_DARP_record: DARP_record,
    db: Db,
) -> Result<impl warp::Reply, Infallible> {
    let mut DARP_records = db.lock().await;

    for DARP_record in DARP_records.iter() {
        if DARP_record.guid == new_DARP_record.guid {
            return Ok(StatusCode::BAD_REQUEST);
        }
    }

    DARP_records.push(new_DARP_record);

    Ok(StatusCode::CREATED)
}

/// Gets a single DARP_record from the data store
///
/// Returns a JSON object of an existing DARP_record. If the DARP_record
/// is not found, it returns a NOT FOUND status code.
/// # Arguments
///
/// * `guid` - String -> the id of the DARP_record to retrieve
/// * `db` - `Db` -> the thread safe data store
pub async fn get_DARP_record(guid: String, db: Db) -> Result<Box<dyn warp::Reply>, Infallible> {
    let DARP_records = db.lock().await;

    for DARP_record in DARP_records.iter() {
        if DARP_record.guid == guid {
            return Ok(Box::new(warp::reply::json(&DARP_record)));
        }
    }

    Ok(Box::new(StatusCode::NOT_FOUND))
}

/// Updates an existing DARP_record
///
/// Overwrites an existing DARP_record in the data store and returns
/// an OK status code. If the DARP_record is not found, a NOT FOUND status
/// code is returned.
///
/// # Arguments
///
/// * `updated_DARP_record` - `DARP_record` -> updated DARP_record info
/// * `db` - `Db` -> thread safe data store
pub async fn update_DARP_record(
    guid: String, 
    updated_DARP_record: DARP_record,
    db: Db,
) -> Result<impl warp::Reply, Infallible> {
    let mut DARP_records = db.lock().await;

    for DARP_record in DARP_records.iter_mut() {
        if DARP_record.guid == guid {
            *DARP_record = updated_DARP_record;
            return Ok(StatusCode::OK);
        }
    }

    Ok(StatusCode::NOT_FOUND)
}

/// Deletes a DARP_record from the data store
///
/// If the DARP_record exists in the data store, the DARP_record is
/// removed and a NO CONTENT status code is returned. If the DARP_record
/// does not exist, a NOT FOUND status code is returned.
///
/// # Arguments
///
/// * `guid` - String -> the id of the DARP_record to delete
/// * `db` - `Db` -> thread safe data store
pub async fn delete_DARP_record(guid: String, db: Db) -> Result<impl warp::Reply, Infallible> {
    let mut DARP_records = db.lock().await;

    let DARP_record_count = DARP_records.len();

    DARP_records.retain(|DARP_record| DARP_record.guid != guid);

    let deleted = DARP_records.len() != DARP_record_count;
    if deleted {
        Ok(StatusCode::NO_CONTENT)
    } else {
        Ok(StatusCode::NOT_FOUND)
    }
}
