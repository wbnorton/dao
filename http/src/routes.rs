use std::convert::Infallible;
use warp::{self, Filter};

use crate::db::Db;
use crate::handlers;
use crate::models::DARP_record;

/// All DARP_record routes
pub fn DARP_record_routes(
    db: Db,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    get_DARP_record(db.clone())
        .or(update_DARP_record(db.clone()))
        .or(delete_DARP_record(db.clone()))
        .or(create_DARP_record(db.clone()))
        .or(DARP_records_list(db))
}

/// GET /DARP_records
fn DARP_records_list(
    db: Db,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path("DARP_records")
        .and(warp::get())
        .and(with_db(db))
        .and_then(handlers::list_DARP_records)
}

/// POST /DARP_records
fn create_DARP_record(
    db: Db,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path("DARP_records")
        .and(warp::post())
        .and(json_body())
        .and(with_db(db))
        .and_then(handlers::create_DARP_record)
}

/// GET /DARP_records/{guid}
fn get_DARP_record(
    db: Db,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("DARP_records" / String)
        .and(warp::get())
        .and(with_db(db))
        .and_then(handlers::get_DARP_record)
}

/// PUT /DARP_records/{guid}
fn update_DARP_record(
    db: Db,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("DARP_records" / String)
        .and(warp::put())
        .and(json_body())
        .and(with_db(db))
        .and_then(handlers::update_DARP_record)
}

/// DELETE /DARP_records/{guid}
fn delete_DARP_record(
    db: Db
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("DARP_records" / String)
        .and(warp::delete())
        .and(with_db(db))
        .and_then(handlers::delete_DARP_record)
}

fn with_db(db: Db) -> impl Filter<Extract = (Db,), Error = Infallible> + Clone {
    warp::any().map(move || db.clone())
}

fn json_body() -> impl Filter<Extract = (DARP_record,), Error = warp::Rejection> + Clone {
    warp::body::content_length_limit(1024 * 16).and(warp::body::json())
}


