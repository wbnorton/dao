use warp;

mod db;
mod handlers;
mod models;
mod routes;

const DARP_DEFAULT_PORT:u16=65014;

#[tokio::main]
async fn main() {
    let db = db::init_db();  //creates or reads database of records
    let DARP_record_routes = routes::DARP_record_routes(db);

    //println!("GOT HERE");

    warp::serve(DARP_record_routes)
        .run(([0, 0, 0, 0], DARP_DEFAULT_PORT ))
        .await;
}
