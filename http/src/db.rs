use std::fs::File;
use std::sync::Arc;

use serde_json::{from_reader,to_writer};
use tokio::sync::Mutex;

use std::time::{SystemTime,UNIX_EPOCH};


use crate::models::DARP_record;

/// Represents an in memory data store of DARP_record data
pub type Db = Arc<Mutex<Vec<DARP_record>>>;

/// Initializes the data store
///
/// Returns a Db type that either contains DARP_record data
/// or is empty.
pub fn init_db() -> Db {
    use gethostname;
    //
    //
    //
    /// get_hostname() - returns the hostname
    /// 
    //pub fn get_hostname(opt:Opt) -> String{
    pub fn get_hostname() -> String{
    //        if opt.hostname=="" {

            let mut geo=String::from(gethostname::gethostname().to_str().unwrap());
            //println!("{}",geo);
            let mut chunks = geo.split('.');
            let mut x = chunks.next().expect("Could not find hostname");
            //println!("{:?}",x.to_string());

            format!("{}",x.to_uppercase().to_string())
    //    } else {
    //        opt.hostname
    //    }
        
    }

    use std::fs::{self};
    //
    /// get_version():returns the Build.YYMMDD.HHMM and Docker.YYMMDD.HHMM filenames from the ../../  (normally $DARPDIR)
    /// 
    pub fn get_version() -> String{
        let mut DARPDIR="../../";
        //should be a fn get_version() -> String {}
        let mut build=String::from("");
        let mut docker=String::from("");
        match fs::read_dir(DARPDIR) {
            Err(why) => println!("! {:?}", why.kind()),
            Ok(paths) => for path in paths {
                let filename=path.unwrap().path();
                //println!("checking filename> {:?}", filename);
                let s = filename.into_os_string().into_string().unwrap();
                let myfs:Vec<&str> = s.split("/").collect();
                //println!("{} {} {}",myfs[0], myfs[1], myfs[2]);
                let b:Vec<&str> = myfs[2].split(".").collect();
                if b[0]=="Build" {
                    build=String::from(myfs[2]);
                    //println!("build={}",myfs[2]);
                }
                if b[0]=="Docker" {
                    docker=String::from(myfs[2]);
                    //println!("docker={}",myfs[2]);                    
                }
            },
        }
        format!("{}:{}",docker,build)
    }
    //
    //
    //  starting

    let filename="./data/DARP_records.json";
    let file = File::open(filename);
            //  now
            ///     return the # of milliseconds elapsed since 1970
            /// 
            pub fn now() -> u64 {       
                let start = SystemTime::now();
                let since_the_epoch = start
                .duration_since(UNIX_EPOCH)
                .expect("Time went backwards");
                
                let unixtime = since_the_epoch.as_millis();
                unixtime as u64
            }
            
            let mut my_ip=String::from("");
            pub fn getip() -> Result<String, Box<dyn std::error::Error>>  {
                let resp = reqwest::blocking::get("http://ifconfig.io/ip")?;
//                .json::<HashMap<String, String>>()?;
                let my_ip=format!("{}",resp.text().unwrap());
                Ok(my_ip)
            }
            my_ip=String::from(getip().unwrap().trim());
            let public_key=String::from("PUBLICKEY_GOES_HERE");

            let mut splitter = get_hostname(); //.splitn(2, '.');
            let first = splitter.splitn(2,'.').next().unwrap();
            //let h=hostname::get()?;
            let geo = String::from(first.to_ascii_uppercase()).clone();
            let group=format!("{}.1",geo);
            let my_guid = format!("{}@{}:{}",public_key,geo,group);
            println!("guid={}",my_guid);

    match file {
        Ok(json) => {
            let DARP_records = from_reader(json).unwrap();
            //println!("Creating DB DARP_records={:?}",DARP_records);
            Arc::new(Mutex::new(DARP_records))
        }

        Err(_) => {
            let my_DARP_Record=DARP_record {
                /// A unique identifier for a customer record
                /// 
                geo:geo,
                /// 
                group:group,
                /// 
                public_key:public_key,
                ///
                mint: String::from("1"),
                /// IP Address to reach node
                ip:my_ip,
                /// Port number to communicate with this node
                port:String::from("65013"),
                /// 
                version:get_version(),
                /// 
                last_pulse_timestamp:String::from("0"),
                /// 
                seq:String::from("0"),
                /// 
                boot_timestamp:String::from(now().to_string()),
                /// 
                owls:String::from(""),
                /// 
                in_pulses:String::from("0"),    
                /// 
                out_pulses:String::from("0"),      
                /// 
                relay_count:String::from("0"), 
                ///
                pkt_drops:String::from("0"), 
                ///
                history:String::from("[0,0,0,]"), 
                ///
                median_history:String::from("[0,0,0.]"),         
                ///
                last_message:String::from("not used"),         
                ///
                state:String::from("init"),         
                ///
                guid:my_guid,
            };
            
                 
            //let DARP_records = File::create("./data/DARP_records.json"); //from_reader(json).unwrap();
            let ary = vec!(my_DARP_Record);
            // write out the file
            use std::io::BufWriter;
            let writer = BufWriter::new(File::create(filename).unwrap());
            serde_json::to_writer_pretty(writer, &ary).unwrap();

            Arc::new(Mutex::new(ary))
        },
    }
}
